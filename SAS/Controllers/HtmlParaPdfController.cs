﻿/*  
    Este arquivo é parte do programa SAS

    SAS it's a free software to generate PDF files.
    
    Este programa é um software livre: você pode redistribuí-lo e/ou
    modificá-lo sob os termos da Licença Pública Geral Affero GNU,
    conforme publicado pela Free Software Foundation, seja a versão 3
    da Licença ou (a seu critério) qualquer versão posterior.

    Este programa é distribuído na esperança de que seja útil,
    mas SEM QUALQUER GARANTIA; sem a garantia implícita de
    COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
    Licença Pública Geral Affero GNU para obter mais detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral Affero GNU
    junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

    contato dsi.dpge@gmail.com
   
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    This file is part of SAS.

    SAS it's a free software to generate PDF files.
    Copyright(C) 2020  DSI

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.

    Contact us dsi.dpge@gmail.com
*/

using iText.Html2pdf;
using SAS.Models;
using SAS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Net.Http.Headers;
using iText.Kernel.Pdf;
using iText.Kernel.Utils;

namespace SAS.Controllers
{
    public class HtmlParaPdfController : ApiController
    {
        // GET: api/HtmlParaPdf
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("versão " + ConfiguracaoSAS.ObterVersaoSistema())
            };
        }
        
        // POST: api/HtmlParaPdf/Pagina
        [HttpPost]
        public HttpResponseMessage Pagina([FromBody] HtmlParaPdfModel model)
        {
            HttpResponseMessage resultado = null;

            try
            {
                using (var MemoryStream = new MemoryStream())
                {
                    HtmlConverter.ConvertToPdf(model.ConteudoHtml, MemoryStream);

                    // Preparação do envio do arquivo
                    resultado = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(MemoryStream.GetBuffer())
                    };
                    resultado.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = model.NomeArquivo
                        };
                    resultado.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                };
            }
            catch (Exception ex)
            {
                resultado = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message)
                };
            }

            return resultado;
        }

        // POST: api/HtmlParaPdf/Paginas
        [HttpPost]
        public HttpResponseMessage Paginas([FromBody] List<HtmlParaPdfModel> model)
        {
            HttpResponseMessage resultado = null;

            try
            {

                using (var memoryStream = new MemoryStream())
                {
                    PdfWriter writer = new PdfWriter(memoryStream);
                    PdfDocument pdf = new PdfDocument(writer);
                    PdfMerger merger = new PdfMerger(pdf);

                    foreach (var item in model)
                    {
                        using (var ms = new MemoryStream())
                        {
                            PdfDocument temp = new PdfDocument(new PdfWriter(ms));
                            HtmlConverter.ConvertToPdf(item.ConteudoHtml, ms);
                            temp = new PdfDocument(new PdfReader(new MemoryStream((byte[])ms.ToArray())));
                            merger.Merge(temp, 1, temp.GetNumberOfPages());
                            temp.Close();
                        }
                    }

                    pdf.Close();

                    // Preparação do envio do arquivo
                    resultado = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(memoryStream.GetBuffer())
                    };
                    resultado.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = "paginas.pdf"/*model.NomeArquivo*/
                        };
                    resultado.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                };
            }
            catch (Exception ex)
            {
                resultado = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message)
                };
            }

            return resultado;
        }

        
    }
}
