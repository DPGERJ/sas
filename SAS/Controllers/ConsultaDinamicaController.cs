﻿/*  
    Este arquivo é parte do programa SAS

    SAS it's a free software to generate PDF files.
    
    Este programa é um software livre: você pode redistribuí-lo e/ou
    modificá-lo sob os termos da Licença Pública Geral Affero GNU,
    conforme publicado pela Free Software Foundation, seja a versão 3
    da Licença ou (a seu critério) qualquer versão posterior.

    Este programa é distribuído na esperança de que seja útil,
    mas SEM QUALQUER GARANTIA; sem a garantia implícita de
    COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
    Licença Pública Geral Affero GNU para obter mais detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral Affero GNU
    junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

    contato dsi.dpge@gmail.com
   
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    This file is part of SAS.

    SAS it's a free software to generate PDF files.
    Copyright(C) 2020  DSI

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.

    Contact us dsi.dpge@gmail.com
*/

using SAS.Models;
using SAS.Util;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SAS.Controllers
{
    public class ConsultaDinamicaController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("versão " + ConfiguracaoSAS.ObterVersaoSistema())
            };
        }

        public HttpResponseMessage Post([FromBody]ConsultaDinamicaModel model)
        {
            HttpResponseMessage resultado = null;
            try
            {

                if (model != null)
                {
                    // Geração do arquivo
                    var geraArquivo = new GeraArquivoConsultaDinamica(model);
                    var byteWorkStream = geraArquivo.GerarPDF();

                    // Preparação do envio do arquivo
                    resultado = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(byteWorkStream)
                    };
                    resultado.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = model.NomeArquivo
                        };
                    resultado.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                }
                else
                {
                    resultado = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                    };
                }
            }
            catch (Exception ex)
            {
                resultado = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message)
                };
            }
            return resultado;
        }
    }
}
