﻿/*  
    Este arquivo é parte do programa SAS

    SAS it's a free software to generate PDF files.
    
    Este programa é um software livre: você pode redistribuí-lo e/ou
    modificá-lo sob os termos da Licença Pública Geral Affero GNU,
    conforme publicado pela Free Software Foundation, seja a versão 3
    da Licença ou (a seu critério) qualquer versão posterior.

    Este programa é distribuído na esperança de que seja útil,
    mas SEM QUALQUER GARANTIA; sem a garantia implícita de
    COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
    Licença Pública Geral Affero GNU para obter mais detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral Affero GNU
    junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

    contato dsi.dpge@gmail.com
   
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    This file is part of SAS.

    SAS it's a free software to generate PDF files.
    Copyright(C) 2020  DSI

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.

    Contact us dsi.dpge@gmail.com
*/

using System;
using System.Collections.Generic;

namespace SAS.Models
{
    public class ExportaDOEModel
    {
        public List<SecaoExportaDOEModel> Secoes { get; set; }
        public List<ExpedienteExportaDOEModel> ExpedienteList { get; set; }
        public string CaminhoImg { get; set; }
        public int Edicao { get; set; }
        public String NomeArquivo { get; set; }
        public DateTime DataDiario { get; set; }
        public string EnderecoArquivo { get; set; }
        public bool Previa { get; set; }
        public bool Colunas { get; set; }


        public ExportaDOEModel()
        {
            Secoes = new List<SecaoExportaDOEModel>();
            ExpedienteList = new List<ExpedienteExportaDOEModel>();
        }

    }

    public class SecaoExportaDOEModel
    {
        public string Secao { get; set; }
        public List<DocumentoExportaDOEModel> Documentos { get; set; }
        public int Ordem { get; set; }

        public SecaoExportaDOEModel()
        {
            Documentos = new List<DocumentoExportaDOEModel>();
        }

    }

    public class DocumentoExportaDOEModel
    {
        public DateTime DataAto { get; set; }
        public string TipoDocumento { get; set; }
        public string Conteudo { get; set; }
        public string idSequencial { get; set; }
        public int idDocumentoSEI { get; set; }
        public string NumeroDocumentoSEI { get; set; }
        public int Ordem { get; set; }
    }

    public class ExpedienteExportaDOEModel
    {
        public string NomeOrgao { get; set; }
        public int Ordem { get; set; }
        //public double Ordem { get; set; }
        public List<String> Pessoas { get; set; }
        //public string Pai { get; set; }

        public ExpedienteExportaDOEModel()
        {
            Pessoas = new List<String>();
        }
    }
}