﻿/*  
    Este arquivo é parte do programa SAS

    SAS it's a free software to generate PDF files.
    
    Este programa é um software livre: você pode redistribuí-lo e/ou
    modificá-lo sob os termos da Licença Pública Geral Affero GNU,
    conforme publicado pela Free Software Foundation, seja a versão 3
    da Licença ou (a seu critério) qualquer versão posterior.

    Este programa é distribuído na esperança de que seja útil,
    mas SEM QUALQUER GARANTIA; sem a garantia implícita de
    COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
    Licença Pública Geral Affero GNU para obter mais detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral Affero GNU
    junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

    contato dsi.dpge@gmail.com
   
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    This file is part of SAS.

    SAS it's a free software to generate PDF files.
    Copyright(C) 2020  DSI

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.

    Contact us dsi.dpge@gmail.com
*/

using iText.Barcodes;
using iText.Html2pdf;
using iText.IO.Font;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Extgstate;
using iText.Kernel.Pdf.Navigation;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Layout;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SAS.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace SAS.Util
{
    public class GeraArquivoDOEIText
    {
        public ExportaDOEModel Model { get; set; }
        public Dictionary<String, Text> sumarioDic;
        public Dictionary<String, int> sumarioDicPaginas;
        public bool Colunas { get; set; }
        #region Estilo
        String style = @"<style type=\""text/css\"">
        p, span {
            font-size: 9pt;  
            font-family: Roboto-Light, Roboto;
        }

        b {
            font-size: 9pt !important;
            font-family: Roboto-Light, Roboto !important;
        }
        
        strong {
            font-size: 9pt !important;
            font-family: Roboto-Light, Roboto !important;
        }

        p.Citacao {
            font-size: 10pt;
            word-wrap: normal;
            margin: 4pt 0 4pt 160px;
            text-align: justify;
        }

        p.Item_Alinea_Letra {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt 6pt 6pt 120px;
            counter-increment: letra_minuscula;
        }

        p.Item_Alinea_Letra:before {
                content: counter(letra_minuscula, lower-latin) \"") \"";
                display: inline-block;
                width: 5mm;
                font-weight: normal;
        }

        p.Item_Inciso_Romano {
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0mm;
            margin: 6pt 6pt 6pt 120px;
            counter-increment: romano_maiusculo;
            counter-reset: letra_minuscula;
        }

        p.Item_Inciso_Romano:before {
            content: counter(romano_maiusculo, upper-roman) \"" - \"";
            display: inline-block;
            width: 15mm;
            font-weight: normal;
        }

        p.Item_Nivel1 {
            text-transform: uppercase;
            font-weight: bold;
            background-color: #e6e6e6;
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0;
            margin: 6pt;
            counter-increment: item-n1;
            counter-reset: item-n2 item-n3 item-n4 romano_maiusculo letra_minuscula;
        }

            p.Item_Nivel1:before {
                content: counter(item-n1) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Item_Nivel2 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: item-n2;
            counter-reset: item-n3 item-n4 romano_maiusculo letra_minuscula;
        }

            p.Item_Nivel2:before {
                content: counter(item-n1) \"".\"" counter(item-n2) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Item_Nivel3 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: item-n3;
            counter-reset: item-n4 romano_maiusculo letra_minuscula;
        }

            p.Item_Nivel3:before {
                content: counter(item-n1) \"".\"" counter(item-n2) \"".\"" counter(item-n3) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Item_Nivel4 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: item-n4;
            counter-reset: romano_maiusculo letra_minuscula;
        }

            p.Item_Nivel4:before {
                content: counter(item-n1) \"".\"" counter(item-n2) \"".\"" counter(item-n3) \"".\"" counter(item-n4) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Paragrafo_Numerado_Nivel1 {
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0mm;
            margin: 6pt;
            counter-increment: paragrafo-n1;
            counter-reset: paragrafo-n2 paragrafo-n3 paragrafo-n4 romano_maiusculo letra_minuscula;
        }

            p.Paragrafo_Numerado_Nivel1:before {
                content: counter(paragrafo-n1) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Paragrafo_Numerado_Nivel2 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: paragrafo-n2;
            counter-reset: paragrafo-n3 paragrafo-n4 romano_maiusculo letra_minuscula;
        }

            p.Paragrafo_Numerado_Nivel2:before {
                content: counter(paragrafo-n1) \"".\"" counter(paragrafo-n2) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Paragrafo_Numerado_Nivel3 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: paragrafo-n3;
            counter-reset: paragrafo-n4 romano_maiusculo letra_minuscula;
        }

            p.Paragrafo_Numerado_Nivel3:before {
                content: counter(paragrafo-n1) \"".\"" counter(paragrafo-n2) \"".\"" counter(paragrafo-n3) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Paragrafo_Numerado_Nivel4 {
            font-size: 9pt;
            text-indent: 0mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
            counter-increment: paragrafo-n4;
            counter-reset: romano_maiusculo letra_minuscula;
        }

            p.Paragrafo_Numerado_Nivel4:before {
                content: counter(paragrafo-n1) \"".\"" counter(paragrafo-n2) \"".\"" counter(paragrafo-n3) \"".\"" counter(paragrafo-n4) \"".\"";
                display: inline-block;
                width: 25mm;
                font-weight: normal;
            }

        p.Tabela_Texto_8 {
            font-size: 8pt;
            text-align: left;
            word-wrap: normal;
            margin: 0 3pt 0 3pt;
        }

        p.Tabela_Texto_Alinhado_Direita {
            font-size: 9pt;
            text-align: right;
            word-wrap: normal;
            margin: 0 3pt 0 3pt;
        }

        p.Tabela_Texto_Alinhado_Esquerda {
            font-size: 9pt;
            text-align: left;
            word-wrap: normal;
            margin: 0 3pt 0 3pt;
        }

        p.Tabela_Texto_Centralizado {
            font-size: 9pt;
            text-align: center;
            word-wrap: normal;
            margin: 0 3pt 0;
        }

        p.Tabela_Texto_Justificado {
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            margin: 0 3pt 0 3pt;
        }

        p.Texto_Alinhado_Direita {
            font-size: 9pt;
            text-align: right;
            word-wrap: normal;
            margin: 6pt;
        }

        p.Texto_Alinhado_Esquerda {
            font-size: 9pt;
            text-align: left;
            word-wrap: normal;
            margin: 6pt;
        }

        p.Texto_Alinhado_Esquerda_Espacamento_Simples {
            font-size: 9pt;
            text-align: left;
            word-wrap: normal;
            margin: 0;
        }

        p.Texto_Alinhado_Esquerda_Espacamento_Simples_Maiusc {
            font-size: 9pt;
            text-align: left;
            text-transform: uppercase;
            word-wrap: normal;
            margin: 0;
        }

        p.Texto_Centralizado {
            font-size: 9pt;
            text-align: center;
            word-wrap: normal;
            margin: 6pt;
        }

        p.Texto_Centralizado_Maiusculas {
            font-size: 10pt;
            text-align: center;
            text-transform: uppercase;
            word-wrap: normal;
        }

        p.Texto_Centralizado_Maiusculas_Negrito {
            font-weight: bold;
            font-size: 10pt;
            text-align: center;
            text-transform: uppercase;
            word-wrap: normal;
        }

        p.Texto_Espaco_Duplo_Recuo_Primeira_Linha {
            letter-spacing: 0.2em;
            font-weight: bold;
            font-size: 9pt;
            text-indent: 25mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
        }

        p.Texto_Esquerda_Maiuscula {
            font-size: 10pt;
            text-align: left;
            text-transform: uppercase;
            word-wrap: normal;
        }

        p.Texto_Esquerda_Negrito_Maiuscula {
            font-weight: bold;
            font-size: 10pt;
            text-align: left;
            text-transform: uppercase;
            word-wrap: normal;
        }

        p.Texto_Fundo_Cinza_Maiusculas_Negrito {
            text-transform: uppercase;
            font-weight: bold;
            background-color: #e6e6e6;
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0;
            margin: 6pt;
        }

        p.Texto_Fundo_Cinza_Negrito {
            font-weight: bold;
            background-color: #e6e6e6;
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0;
            margin: 6pt;
        }

        p.Texto_Justificado {
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0;
            margin: 6pt;
        }

        p.Texto_Justificado_Maiusculas {
            font-size: 9pt;
            text-align: justify;
            word-wrap: normal;
            text-indent: 0;
            margin: 6pt;
            text-transform: uppercase;
        }

        p.Texto_Justificado_Recuo_Primeira_Linha {
            font-size: 9pt;
            text-indent: 25mm;
            text-align: justify;
            word-wrap: normal;
            margin: 6pt;
        }

        p.Texto_Justificado_Recuo_Primeira_Linha_Esp_Simples {
            font-size: 9pt;
            text-indent: 25mm;
            text-align: justify;
            word-wrap: normal;
            margin: 0 0 0 6pt;
        }
        p.Texto_Justificado_Negrito_Maiuscula {font-weight:bold;font-size:10pt;text-align:justify;text-transform:uppercase;word-wrap:normal;}
        p.Texto_Justificado_Recuo_Primeira_Linha {font-size:9pt;text-indent:25mm;text-align:justify;word-wrap:normal;margin:6pt;}
        p.Texto_Justificado_Recuo_Primeira_Linha_Esp_Simples {font-size:9pt;text-indent:25mm;text-align:justify;word-wrap:normal;margin:0 0 0 6pt;}

        // style do SAS
        img{max-width: 710px !important;}
    </style>
";
        #endregion

        public GeraArquivoDOEIText(ExportaDOEModel model, bool colunas = false)
        {
            Model = model;
            Colunas = colunas;
            sumarioDic = new Dictionary<string, Text>();
            sumarioDicPaginas = new Dictionary<string, int>();
        }
        public byte[] GerarPDF(bool previa = false, bool primeiraRodada = true)
        {
            MemoryStream workStream = new MemoryStream();

            PdfWriter writer = new PdfWriter(workStream);

            PdfDocument pdf = new PdfDocument(writer);

            PageSize ps = PageSize.A4;
            // Flush imediato tem que ser falso para criação do sumário
            Document document = new Document(pdf, ps, true);
            document.SetMargins(110, 30, 50, 30);

            CabecalhoEventHandler eventHandler = new CabecalhoEventHandler(document, Model);
            pdf.AddEventHandler(PdfDocumentEvent.START_PAGE, eventHandler);

            if (Model.Previa)
            {
                MarcaEventHandler marcaHandler = new MarcaEventHandler(document);
                pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, marcaHandler);
            }

            RodapeEventHandler rodapeHandler = new RodapeEventHandler(document, Model);
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, rodapeHandler);

            // Layout do arquivo
            float offSet = 20;
            float columnWidth = (ps.GetWidth() - document.GetLeftMargin() - document.GetRightMargin() - offSet) / 2;
            float columnHeight = ps.GetHeight() - document.GetBottomMargin() - document.GetTopMargin();

            Rectangle[] columns = new Rectangle[] {
                    new Rectangle(document.GetLeftMargin(), document.GetBottomMargin(), columnWidth, columnHeight),
                    new Rectangle(document.GetLeftMargin() + offSet + columnWidth, document.GetBottomMargin(), columnWidth, columnHeight)
            };
            Rectangle[] columns2 = new Rectangle[] {
                    new Rectangle(document.GetLeftMargin(),  document.GetBottomMargin(), columnWidth*2 + offSet, columnHeight)
            };
            if (!Colunas)
                document.SetRenderer(new DOERenderer(document, columns, columns2));
            else
                document.SetRenderer(new DOERenderer(document, columns, columns));
            document.SetHyphenation(new iText.Layout.Hyphenation.HyphenationConfig("pt", "br", 3, 3));
            // Inclusão da fonte
            String REGULAR = Model.CaminhoImg + "..\\fonts\\" + ConfiguracaoDOE.FonteDocumento;
            FontProgram fontProgram = FontProgramFactory.CreateFont(REGULAR);
            PdfFont fontRoboto = PdfFontFactory.CreateFont(fontProgram, PdfEncodings.WINANSI, true);

            // Definição de Estilos
            Color verdeDpge = new DeviceRgb(0, 102, 51);
            Color cinzaExpediente = new DeviceRgb(236, 236, 236);
            Color cinzaExpedienteBorda = new DeviceRgb(206, 206, 206);


            Style cabecalho = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(16)
                .SetPaddingTop(3);

            Style texto = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(6)
                .SetPadding(0)
                .SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);

            Style tipoSecao = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(11)
                .SetBold()
                .SetFontColor(verdeDpge)
                .SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);

            Style dataSecao = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(9)
                .SetBold()
                .SetFontColor(verdeDpge)
                .SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);

            Style textoId = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(7)
                .SetBold()
                .SetFontColor(ColorConstants.BLACK)
                .SetTextAlignment(iText.Layout.Properties.TextAlignment.RIGHT);

            Style titulo = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(8)
                .SetPaddingTop(3);

            Style sumarioTexto = new Style()
                .SetFont(fontRoboto)
                .SetFontSize(8)
                .SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);


            PdfOutline outline = pdf.GetOutlines(false);

            PdfOutline expedienteOutline = outline.AddOutline("Expediente");
            expedienteOutline.AddDestination(PdfDestination.MakeDestination(new PdfString("expediente_link")));

            var imgTitulo = new Image(ImageDataFactory.Create(Model.CaminhoImg + ConfiguracaoDOE.ImagemTitulo));
            imgTitulo.SetMaxHeight(16);

            var tabelaExpediente = new Table(new float[] { 1 });
            tabelaExpediente
                .SetBackgroundColor(cinzaExpediente)
                .SetBorder(Border.NO_BORDER)
                .UseAllAvailableWidth();
            var expediente = new Paragraph();
            expediente.Add(imgTitulo);
            expediente.Add("Expediente".ToUpperInvariant()).AddStyle(cabecalho).SetDestination("expediente_link");
            tabelaExpediente.AddCell(new Cell(1, 1).Add(expediente).SetBorder(Border.NO_BORDER));

            var tabelaConteudoExpediente = new Table(new UnitValue[] { UnitValue.CreatePercentValue(1f), UnitValue.CreatePercentValue(98), UnitValue.CreatePercentValue(1f) });
            tabelaConteudoExpediente.UseAllAvailableWidth();
            tabelaConteudoExpediente
                //.SetPadding(0)
                .SetPaddingLeft(4f);
            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(new SolidBorder(cinzaExpedienteBorda, 0.5f)).SetBorderBottom(Border.NO_BORDER));
            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
            foreach (var item in Model.ExpedienteList)
            {

                var pExpediente = new Paragraph();
                pExpediente.Add(item.NomeOrgao).AddStyle(titulo).SetMultipliedLeading(0.8f).SetMarginLeft(10f);
                tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                tabelaConteudoExpediente.AddCell(new Cell().Add(pExpediente).SetBorder(new SolidBorder(cinzaExpedienteBorda, 0.5f)).SetBorderBottom(Border.NO_BORDER).SetBorderTop(Border.NO_BORDER));
                tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                foreach (var chefes in item.Pessoas)
                {
                    var pChefes = new Paragraph();
                    pChefes.Add(chefes).AddStyle(texto).SetMultipliedLeading(0.8f).SetMarginLeft(10f);
                    tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                    tabelaConteudoExpediente.AddCell(new Cell().Add(pChefes).SetBorder(new SolidBorder(cinzaExpedienteBorda, 0.5f)).SetBorderBottom(Border.NO_BORDER).SetBorderTop(Border.NO_BORDER));
                    tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                }


            }

            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));
            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(new SolidBorder(cinzaExpedienteBorda, 0.5f)).SetBorderTop(Border.NO_BORDER));
            tabelaConteudoExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));

            tabelaExpediente.AddCell(new Cell().Add(tabelaConteudoExpediente).SetBorder(Border.NO_BORDER));
            tabelaExpediente.AddCell(new Cell().SetBorder(Border.NO_BORDER));

            document.Add(tabelaExpediente);

            document.Add(new AreaBreak());

            PdfOutline sumarioOutline = outline.AddOutline("Sumário");
            sumarioOutline.AddDestination(PdfDestination.MakeDestination(new PdfString("sumario_link")));
            Div divSumario = new Div();
            divSumario.SetKeepTogether(true);
            var sumario = new Paragraph();
            sumario.Add(imgTitulo);
            sumario.Add("Sumário".ToUpperInvariant()).AddStyle(cabecalho).SetDestination("sumario_link");

            //document.Add(sumario);
            divSumario.Add(sumario);

            PdfOutline secoes = outline.AddOutline("Seções");
            secoes.SetStyle(PdfOutline.FLAG_BOLD);
            int contadorSecao = 0;
            var tabelaSumario = new Table(new float[] { 1, 9 });
            foreach (var item in Model.Secoes.OrderBy(s => s.Ordem))
            {
                var nome = String.Format("secao{0:000}", contadorSecao++);

                PdfOutline kid = secoes.AddOutline(item.Secao);
                kid.AddDestination(PdfDestination.MakeDestination(new PdfString(nome)));

                var celula = new Cell().SetBorder(Border.NO_BORDER);
                var TextNum = new Text(sumarioDicPaginas.ContainsKey(nome) ? sumarioDicPaginas[nome].ToString() : "??");
                var pNumPagina = new Paragraph(TextNum);
                pNumPagina
                    .AddStyle(sumarioTexto)
                    .SetAction(PdfAction.CreateGoTo(nome))
                    .SetBorder(new SolidBorder(0.5f))
                    .SetPadding(1.2f)
                    .SetPaddingTop(0)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .SetPaddingBottom(0);
                celula.Add(pNumPagina);
                tabelaSumario.AddCell(celula);
                if (!sumarioDic.ContainsKey(nome))
                {
                    sumarioDic.Add(nome, TextNum);
                }

                celula = new Cell().SetBorder(Border.NO_BORDER);
                var p = new Paragraph()
                    .AddStyle(sumarioTexto)
                    .SetAction(PdfAction.CreateGoTo(nome))
                    .Add(item.Secao.ToUpperInvariant());
                celula.Add(p).SetVerticalAlignment(VerticalAlignment.MIDDLE);
                tabelaSumario.AddCell(celula);

            }
            divSumario.Add(tabelaSumario);

            #region ImagemLevi
            Image fundoSumario = new Image(ImageDataFactory.Create(Model.CaminhoImg + ConfiguracaoDOE.ImagemSumario));

            Rectangle tamanhoSumario = new Rectangle(document.GetPdfDocument().GetPage(1).GetPageSize().GetWidth() / 2, -150, document.GetPdfDocument().GetPage(1).GetPageSize().GetWidth(), document.GetPdfDocument().GetPage(1).GetPageSize().GetHeight());
            PdfCanvas pdfCanvasSumario = new PdfCanvas(document.GetPdfDocument().GetFirstPage());
            pdfCanvasSumario.SaveState();
            Canvas canvasSumario = new Canvas(pdfCanvasSumario, document.GetPdfDocument(), tamanhoSumario);
            canvasSumario.Add(fundoSumario.ScaleAbsolute(tamanhoSumario.GetWidth() / 2 - 30, tamanhoSumario.GetHeight() - 200));
            pdfCanvasSumario.RestoreState();
            pdfCanvasSumario.Release();
            #endregion

            document.Add(divSumario);

            //Quebra de página para imagem do Levi
            document.Add(new AreaBreak());

            contadorSecao = 0;
            // Preenchimento do arquivo com os documentos
            foreach (var item in Model.Secoes.OrderBy(s => s.Ordem))
            {
                item.Documentos = item.Documentos.OrderBy(d => d.Ordem).ToList();
                var nome = String.Format("secao{0:000}", contadorSecao++);
                var paragrafo = new Paragraph();
                paragrafo.Add(imgTitulo);
                paragrafo.Add(item.Secao).AddStyle(cabecalho);
                paragrafo.SetNextRenderer(new AtualizaSumarioRenderer(paragrafo, sumarioDic[nome], sumarioDicPaginas, nome));
                paragrafo.SetDestination(nome);
                //document.Add(new Paragraph(item.Secao).AddStyle(cabecalho));
                document.Add(paragrafo);

                foreach (var tipo in item.Documentos.Select(i => i.TipoDocumento).Distinct())
                {
                    document.Add(new Paragraph(tipo).AddStyle(tipoSecao));

                    // Agrupa por Data
                    foreach (var data in item.Documentos.Where(i => i.TipoDocumento.Equals(tipo)).Select(d => d.DataAto).Distinct().OrderBy(d => d.Year).ThenBy(d => d.Month).ThenBy(d => d.Day))
                    {
                        document.Add(new Paragraph("| De " + data.ToString("dd.MM.yyyy")).AddStyle(dataSecao));
                        foreach (var doc in item.Documentos.Where(d => d.DataAto == data && d.TipoDocumento.Equals(tipo)))
                        {
                            // O ideal seria fazer esse tratamento pelo style do CSS
                            // mas por algum motivo não funcionou
                            String documentoTratado = doc.Conteudo;
                            if (Colunas)
                                documentoTratado = doc.Conteudo.Replace("<img ", "<img style='max-width: 335px;' ");
                            else
                                documentoTratado = doc.Conteudo.Replace("<img ", "<img style='max-width: 710px;' ");

                            var elementos = HtmlConverter.ConvertToElements(style + documentoTratado);
                            foreach (var e in elementos)
                            {
                                if (e is Table)
                                {
                                    Table a = (Table)e;
                                    a.UseAllAvailableWidth();
                                    document.Add(a);
                                }
                                else if (e is Paragraph)
                                {
                                    Paragraph p = (Paragraph)e;
                                    p.AddStyle(texto);
                                    var filhos = p.GetChildren();
                                    bool apenasVazio = true;
                                    foreach (var text in filhos)
                                    {
                                        if (text is Text)
                                        {
                                            var t = (Text)text;
                                            if (!String.IsNullOrWhiteSpace(t.GetText()))
                                                apenasVazio = false;
                                        }
                                        else
                                        {
                                            apenasVazio = false;
                                        }
                                    }
                                    if (!apenasVazio)
                                        document.Add(p);
                                }
                                else
                                {
                                    document.Add((IBlockElement)e);
                                }

                            }
                            document.Add(new Paragraph("Id: " + doc.idSequencial + " - Protocolo: " + doc.NumeroDocumentoSEI).AddStyle(textoId));
                        }
                    }
                }

            }


            // Faz pegar as alterações no documento antes de renderizar o mesmo
            // É necessário para a montagem do sumário.
            //document.Relayout();

            //            document.Flush();
            document.Close();


            if (primeiraRodada)
            {
                workStream.Close();
                return GerarPDF(false, false);
            }
            else
            {
                return workStream.ToArray();
            }


        }


        protected class AtualizaSumarioRenderer : ParagraphRenderer
        {
            protected Text paragrafo;
            protected Dictionary<String, int> nomeDic;
            protected String nome;
            public AtualizaSumarioRenderer(Paragraph modelElement, Text paragrafo, Dictionary<String, int> nomeDic, String nome) : base(modelElement)
            {

                this.paragrafo = paragrafo;
                this.nomeDic = nomeDic;
                this.nome = nome;
            }

            public override LayoutResult Layout(LayoutContext layoutContext)
            {
                LayoutResult result = base.Layout(layoutContext);

                paragrafo.SetText(layoutContext.GetArea().GetPageNumber().ToString());
                if (!nomeDic.ContainsKey(nome))
                {
                    nomeDic[nome] = layoutContext.GetArea().GetPageNumber();
                }

                return result;
            }
        }

        protected class CabecalhoEventHandler : IEventHandler
        {
            protected Document doc { get; set; }
            protected Image imgCabecalho;
            protected Image linhaCabecalho;
            protected BarcodeQRCode QrCode;
            protected ExportaDOEModel Model;
            protected String dataPublicacao;

            public CabecalhoEventHandler(Document doc, ExportaDOEModel Model)
            {
                this.doc = doc;
                this.Model = Model;
                imgCabecalho = new Image(ImageDataFactory.Create(Model.CaminhoImg + ConfiguracaoDOE.ImagemLogo));

                linhaCabecalho = new Image(ImageDataFactory.Create(Model.CaminhoImg + ConfiguracaoDOE.ImagemFaixaCabecalho));

                this.QrCode = new BarcodeQRCode(Model.EnderecoArquivo + Model.NomeArquivo);

                CultureInfo culture = new CultureInfo("pt-BR");
                DateTimeFormatInfo dtfi = culture.DateTimeFormat;
                int dia = Model.DataDiario.Day;
                int ano = Model.DataDiario.Year;
                string mes = culture.TextInfo.ToTitleCase(dtfi.GetMonthName(Model.DataDiario.Month));
                string diasemana = culture.TextInfo.ToTitleCase(dtfi.GetDayName(Model.DataDiario.DayOfWeek));
                string data = "Publicação: " + diasemana + ", " + dia + " de " + mes + " de " + ano;
                this.dataPublicacao = data;

            }

            public void HandleEvent(Event evento)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)evento;
                var pagina = docEvent.GetPage();
                var documento = pagina.GetDocument();
                int paginaNum = documento.GetPageNumber(pagina);

                PdfDocument pdfDoc = docEvent.GetDocument();

                PdfCanvas pdfCanvas = new PdfCanvas(docEvent.GetPage());
                Rectangle pageSize = docEvent.GetPage().GetPageSize();

                var tamanhoLinha = 50;
                //Espaço para preenchimento do cabeçalho
                var rectHeader = new Rectangle(doc.GetLeftMargin(),
                                               pageSize.GetTop() - doc.GetTopMargin() - tamanhoLinha - 22,
                                               pageSize.GetWidth() - (doc.GetRightMargin() + doc.GetLeftMargin()),
                                               doc.GetTopMargin() + tamanhoLinha);

                string textoDesde = "DESDE 25 DE NOVEMBRO DE 2019";
                var tituloDesde = new Paragraph(textoDesde)
                    .SetFontColor(ColorConstants.BLACK)
                    .SetItalic()
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetFontSize(8).SetFixedPosition(doc.GetLeftMargin() + 2, pageSize.GetTop() - doc.GetTopMargin() + 27, 200);

                var tituloRelatorio = new Paragraph("Edição N.º " + Model.Edicao.ToString("000") + " / " + Model.DataDiario.Year)
                    .SetFontColor(ColorConstants.WHITE)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetFontSize(9).SetFixedPosition(doc.GetLeftMargin() + 2, pageSize.GetTop() - doc.GetTopMargin() + 10, 100);

                var publicacao = new Paragraph(dataPublicacao)
                    .SetFontColor(ColorConstants.WHITE)
                    .SetTextAlignment(TextAlignment.RIGHT)
                    .SetFontSize(8).SetFixedPosition(doc.GetLeftMargin() + 5 + 100, pageSize.GetTop() - doc.GetTopMargin() + 10, 420);

                ////Canvas para o logo da defensoria
                var canvas = new Canvas(pdfCanvas, pdfDoc, rectHeader);

                canvas.Add(imgCabecalho).SetTextAlignment(TextAlignment.CENTER);
                canvas.Add(linhaCabecalho).SetTextAlignment(TextAlignment.CENTER);
                canvas.Add(tituloDesde);
                canvas.Add(tituloRelatorio);
                canvas.Add(publicacao);
                var qrCodeImg = new Image(QrCode.CreateFormXObject(ColorConstants.BLACK, pdfDoc))
                        .SetFixedPosition(pageSize.GetRight() - doc.GetRightMargin() - 70, pageSize.GetTop() - 70, 60);

                //.SetBorder(new SolidBorder(1));
                canvas.Add(qrCodeImg);



            }

        }

        protected class RodapeEventHandler : IEventHandler
        {
            protected Document doc;
            protected DateTime DataCriacao { get; set; }
            protected ExportaDOEModel Model;
            protected Image imgRodape;

            public RodapeEventHandler(Document doc, ExportaDOEModel Model)
            {
                this.doc = doc;
                this.Model = Model;
                imgRodape = new Image(ImageDataFactory.Create(Model.CaminhoImg + ConfiguracaoDOE.ImagemRodape));

            }

            public void HandleEvent(Event evento)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)evento;
                var pagina = docEvent.GetPage();
                var documento = pagina.GetDocument();
                int paginaNum = documento.GetPageNumber(pagina);


                PdfDocument pdfDoc = docEvent.GetDocument();

                PdfCanvas pdfCanvas = new PdfCanvas(docEvent.GetPage());
                Rectangle pageSize = docEvent.GetPage().GetPageSize();

                var tamanhoLinha = 0;
                //Espaço para preenchimento do cabeçalho
                var rectFooter = new Rectangle(doc.GetLeftMargin() - 6,
                                               pageSize.GetBottom(),
                                               pageSize.GetWidth() - (doc.GetRightMargin() + doc.GetLeftMargin() - 14),
                                               doc.GetBottomMargin() + tamanhoLinha);

                var rectPagina = new Rectangle(pageSize.GetRight() - doc.GetRightMargin() - 39,
                               pageSize.GetBottom() + 12,
                               50,
                               20);

                ////Canvas para as imagens do rodapé
                var canvas = new Canvas(pdfCanvas, pdfDoc, rectFooter);

                canvas.Add(imgRodape).SetTextAlignment(TextAlignment.CENTER);

                ////Canvas para as imagens do rodapé
                var canvasNumPagina = new Canvas(pdfCanvas, pdfDoc, rectPagina);
                var pPagina = new Paragraph(paginaNum.ToString())
                    .SetFontColor(ColorConstants.WHITE)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .SetFontSize(10);

                canvasNumPagina.Add(pPagina);


            }
        }

        protected class MarcaEventHandler : IEventHandler
        {
            protected Document doc;
            //protected DateTime DataCriacao { get; set; }
            //protected ExportaDOEModel Model;
            //protected Image img;

            Style estiloPrevio = new Style()
            //.SetFont(fontRoboto)
            .SetFontSize(60)
            .SetFontColor(ColorConstants.RED);

            public MarcaEventHandler(Document doc/*, ExportaDOEModel Model*/)
            {
                this.doc = doc;

            }

            public void HandleEvent(Event evento)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)evento;
                var pagina = docEvent.GetPage();
                var documento = pagina.GetDocument();
                int paginaNum = documento.GetPageNumber(pagina);


                PdfDocument pdfDoc = docEvent.GetDocument();

                PdfCanvas pdfCanvas = new PdfCanvas(docEvent.GetPage());
                Rectangle pageSize = new Rectangle(pagina.GetPageSize().GetWidth() / 2, pagina.GetPageSize().GetBottom() / 2, pagina.GetPageSize().GetWidth(), pagina.GetPageSize().GetHeight());

                var tranState = new PdfExtGState();
                tranState.SetFillOpacity(0.5f);

                PdfCanvas tela = new PdfCanvas(pagina);
                tela.SaveState().SetExtGState(tranState);


                Canvas canvas = new Canvas(tela, documento, pageSize);

                Paragraph pPrevia = new Paragraph().Add("ARQUIVO DE PRÉVIA").AddStyle(estiloPrevio);
                canvas.ShowTextAligned(pPrevia, pageSize.GetWidth() / 2, pageSize.GetHeight() / 2, 1, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 45);

                tela.RestoreState();
            }
        }

        /// <summary>
        /// Faz a mudança do layout de 2 colunas para 1 coluna só
        /// na segunda página
        /// </summary>
        protected class DOERenderer : ColumnDocumentRenderer
        {

            Rectangle[] SegundaColuna;

            public DOERenderer(Document document, Rectangle[] coluna1, Rectangle[] coluna2, bool flush = true) : base(document, flush, coluna1)
            {
                this.SegundaColuna = coluna2;
            }

            protected override PageSize AddNewPage(PageSize customPageSize)
            {
                // A partir da segunda página começa a usar a segunda forma 
                // de coluna
                if (base.currentPageNumber == 2)
                    base.columns = SegundaColuna;

                return base.AddNewPage(customPageSize);
            }


        }
    }
}