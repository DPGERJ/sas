﻿/*  
    Este arquivo é parte do programa SAS

    SAS it's a free software to generate PDF files.
    
    Este programa é um software livre: você pode redistribuí-lo e/ou
    modificá-lo sob os termos da Licença Pública Geral Affero GNU,
    conforme publicado pela Free Software Foundation, seja a versão 3
    da Licença ou (a seu critério) qualquer versão posterior.

    Este programa é distribuído na esperança de que seja útil,
    mas SEM QUALQUER GARANTIA; sem a garantia implícita de
    COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
    Licença Pública Geral Affero GNU para obter mais detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral Affero GNU
    junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.

    contato dsi.dpge@gmail.com
   
    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    This file is part of SAS.

    SAS it's a free software to generate PDF files.
    Copyright(C) 2020  DSI

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.

    Contact us dsi.dpge@gmail.com
*/

using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using SAS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SAS.Util
{
    public class GeraArquivoConsultaDinamica
    {
        private ConsultaDinamicaModel Model;

        public GeraArquivoConsultaDinamica(ConsultaDinamicaModel model)
        {
            this.Model = model;
        }

        internal byte[] GerarPDF()
        {
            //baseado em https://developers.itextpdf.com/examples/tables/clone-101-very-simple-table

            MemoryStream ms = new MemoryStream();
            PdfWriter writer = new PdfWriter(ms);
            PdfDocument pdfDoc = new PdfDocument(writer);
            Document document = new Document(pdfDoc, PageSize.A4.Rotate());
            document.SetMargins(60, 20, 20, 20);

            Style textoTabela = new Style()
                                .SetFontSize(10);

            var colunas = Model.Colunas.Count();

            Table table = new Table(colunas,true);

            Cell cel = null;

            //Handlers para o rodapé e cabeçalho
            var eventoRodape = new RodapeEventHandler(document, DateTime.Now);
            var eventoCabecalho = new CabecalhoEventHandler(document, Model);

            //Adiciona um event handler ao documento para cada inicio e fim de página. 
            pdfDoc.AddEventHandler(PdfDocumentEvent.START_PAGE, eventoCabecalho);
            pdfDoc.AddEventHandler(PdfDocumentEvent.END_PAGE, eventoRodape);

            //Adiciona a tabela ao documento primeiro, para melhorar o desempenho em tabelas grandes.
            //Referência: https://developers.itextpdf.com/examples/tables/clone-large-tables 
            document.Add(table);

            //geração do cabeçalho da tabela
            foreach (var item in Model.Colunas)
            {
                var titulo = new Text(item);

                //É necessário um parágrafo novo a cada campo adicionado.
                var paragrafoTitulo = new Paragraph(titulo)
                    .SetFontColor(new DeviceRgb(0, 114, 47))
                    //.SetBold()
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetFontSize(10);

                cel = new Cell()
                    .Add(paragrafoTitulo)
                    //borda da célula(no caso, somente a borda inferior)
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetBorderBottom(new SolidBorder(new DeviceRgb(0, 114, 47), 1))
                    .SetBorder(Border.NO_BORDER);

                //Adicionando a célula à tabela. HeaderCell significa que a célula será renderizada no header da tabela de cada página.
                table.AddHeaderCell(cel);
            }

            //variável de controle da coloração de fundo da linha
            int linha = 0;

            //preenchimento dos dados na tabela
            foreach (IEnumerable<String> Itens in Model.Linhas)
            {
                //a cada 5 linhas preenchidas, adiciona as novas linhas ao documento (aumenta o desempenho em tabelas grandes)
                if (linha % 5 == 0)
                {
                    table.FlushContent();
                }

                //cor de fundo 
                Color corDeFundo = ColorConstants.WHITE;
                if (linha % 2 == 1)
                {
                    corDeFundo = new DeviceRgb(240,240,240);
                }

                linha++;

                //preenchimento de acordo com as colunas
                for (int i = 0; i < colunas; i++)
                {
                    //preenchimento da célula de conteúdo
                    var texto = Itens.ElementAt(i);
                    texto = texto.Replace("<br/>", "\n");

                    cel = new Cell()
                        .Add(new Paragraph().Add(texto).AddStyle(textoTabela))
                        .SetBorder(Border.NO_BORDER)
                        .SetBackgroundColor(corDeFundo)
                        .SetHorizontalAlignment(HorizontalAlignment.CENTER);

                    table.AddCell(cel);
                }
            }

            table.SetAutoLayout();
            //percentagem da largura da tabela em relação à página
            table.SetWidth(new UnitValue(UnitValue.PERCENT, 98));
            table.SetMaxWidth(new UnitValue(UnitValue.PERCENT, 98));

            //fecha a edição da tabela
            table.Complete();



            //Insere o total de páginas no rodapé de cada página.
            eventoRodape.InsereTotalPaginas(pdfDoc);

            //adiciona a data de criação 
            pdfDoc.GetDocumentInfo().AddCreationDate();

            //fecha o documento
            document.Close();

            //retorna o byte array
            return ms.GetBuffer();
        }

        #region EventHandlers iText7
        //Criação de eventos para rodapé e cabeçalho
        //https://developers.itextpdf.com/examples/page-events/clone-page-events-headers-and-footers
        //Inserção de imagens
        //https://developers.itextpdf.com/content/itext-7-examples/itext-7-image-examples
        private class CabecalhoEventHandler : IEventHandler
        {
            protected Document doc;
            protected Image logoDefensoria;
            protected string tituloConsulta;

            public CabecalhoEventHandler(Document doc, ConsultaDinamicaModel Model)
            {
                this.doc = doc;
                this.tituloConsulta = Model.ConsultaTitulo;

                var uriLogoDefensoria = new Uri(Model.CaminhoImg + "novologodpge.png");
                logoDefensoria = new Image(ImageDataFactory.Create(uriLogoDefensoria));

                //Width - 227, Height - 49
                logoDefensoria.ScaleAbsolute(227, 49);
            }

            public void HandleEvent(Event evento)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)evento;
                PdfDocument pdfDoc = docEvent.GetDocument();

                PdfCanvas canvas = new PdfCanvas(docEvent.GetPage());
                Rectangle pageSize = docEvent.GetPage().GetPageSize();

                //Espaço para preenchimento do cabeçalho
                var rectHeader = new Rectangle(doc.GetLeftMargin(),
                                               pageSize.GetTop() - doc.GetTopMargin() - 10,
                                               pageSize.GetWidth() - (doc.GetRightMargin() + doc.GetLeftMargin() + 15),
                                               doc.GetTopMargin());

                var tituloRelatorio = new Paragraph("Relatório de " + tituloConsulta)
                    .SetTextAlignment(TextAlignment.RIGHT)
                    .SetFontSize(15);

                //Canvas para a logo
                new Canvas(canvas, pdfDoc, rectHeader)
                    .Add(logoDefensoria);

                //Canvas para o texto do título
                new Canvas(canvas, pdfDoc, rectHeader)
                    .Add(tituloRelatorio);
            }
        }

        private class RodapeEventHandler : IEventHandler
        {
            protected Document doc;
            protected PdfFormXObject placeholderTotalPaginas;
            protected DateTime DataCriacao { get; set; }

            public RodapeEventHandler(Document doc, DateTime dataCriacao)
            {
                this.doc = doc;
                DataCriacao = dataCriacao;

                placeholderTotalPaginas = new PdfFormXObject(new Rectangle(0, 0, 30, 20));
            }

            public void HandleEvent(Event evento)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)evento;
                PdfDocument pdfDoc = docEvent.GetDocument();
                PdfCanvas PdfCanvas = new PdfCanvas(docEvent.GetPage());
                PdfPage page = docEvent.GetPage();
                Rectangle pageSize = page.GetPageSize();

                //Adiciona ao Canvas do PDF o objeto que irá receber o total de páginas no fim da geração do PDF.
                PdfCanvas.AddXObject(placeholderTotalPaginas, pageSize.GetWidth() - 61, pageSize.GetBottom() + 5);

                //Espaço para preenchimento do rodapé
                var rectRodape = new Rectangle(doc.GetLeftMargin(),
                                                     pageSize.GetBottom() + 5,
                                                     pageSize.GetWidth() - (doc.GetRightMargin() + doc.GetLeftMargin() + 45),
                                                     doc.GetBottomMargin());

                var emitidoEm = string.Format("Emitido em: {0}", DataCriacao);
                var numeroPagina = string.Format("Página {0} de ", pdfDoc.GetPageNumber(page));

                //Canvas para o texto do "Emitido em: "
                new Canvas(PdfCanvas, pdfDoc, rectRodape)
                    .Add(new Paragraph(emitidoEm).SetFontSize(9));

                //Canvas para o texto do "Página x de y"
                new Canvas(PdfCanvas, pdfDoc, rectRodape)
                    .Add(new Paragraph(numeroPagina).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(9));

            }

            /// <summary>
            /// Método para inserir o número total de páginas no objeto (placeholderTotalPaginas) criado anteriormente.
            /// </summary>
            /// <param name="pdfDoc"></param>
            public void InsereTotalPaginas(PdfDocument pdfDoc)
            {
                //Canvas com o numero total de páginas renderizadas após o fim do preenchimento do pdf.
                new Canvas(placeholderTotalPaginas, pdfDoc)
                    .Add(new Paragraph(pdfDoc.GetNumberOfPages().ToString()).SetFontSize(9));
            }
        }

        #endregion
    }
}