# SAS - Serviços de Apoio ao SCI

## Página do projeto

Para mais informações, incluindo código fonte e documentação, visite a página do projeto:

https://gitlab.com/DPGERJ/sas

## Introdução

O SAS é uma API destinada para apoio ao sistema SCI da Defensoria Pública do Estado do Rio de Janeiro, lançado sob a licença GNU AFFERO GENERAL PUBLIC LICENSE v3.0 (AGPL-3.0), com o intuito inicial de realizar a geração de arquivos PDF utilizando a ferramenta iTextPDF 

## Principais Funcionalidades

* Geração de PDF do Diário Oficial Eletrônico da Defensoria
* Geração de PDF da exportação das páginas de consulta do SCI (Sistema interno da instituição)

## Arquivos de Configuração

A configuração de algumas constantes, como nomes de imagens, podem ser necessárias nos arquivos:
* Util/ConfiguracaoDOE.cs
* Util/ConfiguracaoConsultaDinamica.cs

## Utilização

* **/api/diariooficial:**

Chamar enviando por POST os dados da model [ExportaDOEModel](SAS/Models/ExportaDOEModel.cs), que retornará um Byte Array do arquivo gerado.

* **/api/consultadinamica:**

Chamar enviando por POST os dados da model [ConsultaDinamicaModel](SAS/Models/ConsultaDinamicaModel), que retornará um Byte Array do arquivo gerado.

## Dependências

Algumas bibliotecas que utilizamos:

* [Newtonsoft JSON](https://www.newtonsoft.com/json)
* [iText PDF 7 AGPL-version](https://itextpdf.com/)

## Autores

Desenvolvido pela Coordenação de Sistemas de Informação (COSIS) da Defensoria Pública do Estado do Rio de Janeiro 